package com.db.aplicaciondeclase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MenuPrincipal extends AppCompatActivity implements View.OnClickListener {
    private Button btnIMC;
    private Button btnConversor;
    private Button btnCalculadora;
    private EditText textNombre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        btnIMC = findViewById(R.id.btnCalculadoraIMC);
        //Creación de los elementos lógicos
        btnConversor = findViewById(R.id.btnConversor);
        btnCalculadora = findViewById(R.id.btnCaculadora);
        textNombre = findViewById(R.id.textNombreUsuario);
        btnIMC.setOnClickListener(this);
        btnConversor.setOnClickListener(this);
        btnCalculadora.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnCalculadoraIMC:
                intent  = new Intent(this, IndiceMasaCorporal.class);
                // envio de datos
                intent.putExtra("nombreUsuario", textNombre.getText().toString());
                startActivity(intent);

                break;
            case R.id.btnConversor:
                intent  = new Intent(this, ConversorUnidades.class);
                intent.putExtra("nombreUsuario", textNombre.getText().toString());
                startActivity(intent);
                break;
            case R.id.btnCaculadora:
                intent  = new Intent(this, Calculadora.class);

                startActivity(intent);
                break;
        }

    }
}