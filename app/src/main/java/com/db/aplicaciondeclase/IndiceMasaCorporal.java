package com.db.aplicaciondeclase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class IndiceMasaCorporal extends AppCompatActivity implements View.OnClickListener {
    ImageView imagenEstado;
    EditText textAltura;
    EditText textPeso;
    Button btnCalcularImc;
    TextView txtResultado;
    private String nombreUsuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indice_masa_corporal);
        Intent intent = getIntent();
        nombreUsuario = intent.getStringExtra("nombreUsuario");
        imagenEstado = findViewById(R.id.imageViewIMC);
        textAltura = findViewById(R.id.editTextAlturaIMC);
        textPeso = findViewById(R.id.editTextPesoIMC);
        txtResultado = findViewById(R.id.txtResultado);
        btnCalcularImc = findViewById(R.id.btnDeCalculoImc);
        btnCalcularImc.setOnClickListener(this);

        Toast.makeText(this, "Bienvenido a su cálculo de IMC" +nombreUsuario, Toast.LENGTH_SHORT).show();
    }

    public Double calcularIMC (double peso, double altura){
        double  imc = 0;
        imc = peso / (altura*altura);
        return  imc;
    }

    @Override
    public void onClick(View var1) {

        switch (var1.getId()){
            case R.id.btnDeCalculoImc:{
                // Toast.makeText(this, "Entro: ", Toast.LENGTH_LONG).show();
                Double peso = Double.parseDouble(textPeso.getText().toString());
                Double altura = Double.parseDouble(textAltura.getText().toString());
                Double imc = calcularIMC(peso, altura);
               // Toast.makeText(this, "Su IMC es: "+ imc, Toast.LENGTH_LONG).show();
                txtResultado.setText("Su IMC es: "+ imc);

            }
        }
    }
}